# JfinalSwagger
fork自：提到的项目：【[https://gitee.com/zdkhqh/JfinalSwagger](https://gitee.com/zdkhqh/JfinalSwagger)】

使用时，在【[http://www.jfinal.com/share/1569]( http://www.jfinal.com/share/1569)】的基础上需要在Route的配置中将需要识别的controller按照下面的格式配置：

``` java
me.add(new Routes() {
            @Override
            public void config() {
                add("/customer", CustomerController.class);
                add("/device", DeviceController.class);
            }
        });
```

20200209修复post接口时不能识别body传参的bug。